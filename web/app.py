from tornado import websocket, web, ioloop, options
import json
import torndb
import jsontemplate

####### DEFAULT CONFIG #######

#### config - separated ####
options.define("config_file", default="../config/config.conf")

#### config - db ####
options.define("host", default="10.1.1.10:3306")
options.define("db", default="las")
options.define("username", default="root")
options.define("password", default="kompot123")
#### config - server ####
options.define("port", default="8888")

####### SERVER #######

#clients grouped by alarms two dimensional array
clients = {}

class IndexHandler(web.RequestHandler):
    def get(self, *args):
        self.render("../views/index.html")

class SocketHandler(websocket.WebSocketHandler):

    def open(self, *args):
        #get alarm id, user id and token
        self.alarmId = int(self.get_argument("alarmId")) #alarm id
        self.userToken = str(self.get_argument("userToken")) #alarm id
        self.userId = int(self.get_argument("userId")) #alarm id

        #verify if token is valid
        query = """
            SELECT t.user_id
            FROM las_token as t
            WHERE t.apiToken=\""""+str(self.userToken)+"""\" LIMIT 1"""
        result = db.get(query)

        #query for checking if user have permissions to that alarm
        queryUserGroups = """
            SELECT au.alarm_id
            FROM fos_user_user_group as uu
            LEFT JOIN las_alarm_user_group as au ON uu.group_id = au.group_id
            WHERE uu.user_id = """+str(self.userId)+""" and alarm_id = """+str(self.alarmId)+""" LIMIT 1"""

        if not bool(result) or result.user_id != self.userId or not db.execute_rowcount(queryUserGroups):
            print 'Refused connection: '+'userId: '+str(self.userId)+' userToken: '+str(self.userToken)+' alarmId: '+str(self.alarmId)
            self.close()
        else:
            #open connection
            print 'New connection: '+'userId: '+str(self.userId)+' userToken: '+str(self.userToken)+' alarmId: '+str(self.alarmId)

            if self.alarmId not in clients:
                clients[self.alarmId] = []
            if self not in clients[self.alarmId]:
                clients[self.alarmId].append(self)

    def on_close(self):
        if self in clients[self.alarmId]:
            clients[self.alarmId].remove(self)

class ApiHandler(web.RequestHandler):

    @web.asynchronous
    def get(self, *args):
        self.finish()

        #"what happened" parameters
        alarmId = int(self.get_argument("alarmId"))
        activityId = int(self.get_argument("activityId"))

        #getting data about specific activity
        query = """
            SELECT f.*, u.*, a.*
            FROM las_alarm_activity as a
            LEFT JOIN fos_user as u ON a.creator_id = u.id
            LEFT JOIN las_file as f ON u.picture_id = f.id
            WHERE a.id="""+str(activityId)+""" LIMIT 1"""
        activityEntity = db.get(query)

        #generate JSON from template
        with open('../views/activity.json', 'r') as content_file:
            content = content_file.read()

        #fill template with data from db
        keys_type = {
            2 : 'ON_MY_WAY',
            4 : 'NOT_ON_LOCATION',
            8 : 'FALSE_ALARM',
            16 : 'EMERGENCY_WORKERS',
            32 : 'CLOSE_ALARM',
        }
        content_data = {
            'alarmId': alarmId,
            'activityId': activityId,
            'activityCreatorId': str(activityEntity.creator_id),
            'activityCreatorEmail': activityEntity.email,
            'activityCreatorName': activityEntity.name,
            'activityCreatorPhoneNumber': activityEntity.phone_number,
            'activityCreatorAddress': activityEntity.address,
            'activityCreatorZipCode': activityEntity.zip_code,
            'activityCreatorCity': activityEntity.city,
            'activityCreatorChangePassword': 'true' if bool(activityEntity.change_password) else 'false',
            'activityCreatorChangedEmail': 'true' if bool(activityEntity.changed_email) else 'false',
            'activityCreatorLongitude': str(int(float(activityEntity.longitude) * 1000000)) if bool(activityEntity.longitude) else activityEntity.longitude,
            'activityCreatorLatitude': str(int(float(activityEntity.latitude) * 1000000)) if bool(activityEntity.latitude) else activityEntity.latitude,
            'activityCreatedAt': str(activityEntity.createdAt),
            'activityType': keys_type[activityEntity.type] if bool(activityEntity.type) else activityEntity.type,
            'activityDiscriminator': activityEntity.discriminator,
            'activityContent': activityEntity.content
        }

        if (activityEntity.picture_id):
            content_data['activityCreatorPicturePath'] = activityEntity.path
            content_data['activityCreatorPictureThumbnailPath'] = activityEntity.thumbnail_path

        data = jsontemplate.expand(
            content,
            content_data
        )

        #sending message to clients (only selected Alarm)
        if alarmId in clients:
            for c in clients[alarmId]:
                c.write_message(data)

    @web.asynchronous
    def post(self):
        pass

app = web.Application([
    (r'/', IndexHandler),
    (r'/ws', SocketHandler),
    (r'/api', ApiHandler),
    #(r'/(rest_api_example.png)', web.StaticFileHandler, {'path': './'}),
])

if __name__ == '__main__':
    options.parse_config_file(options.options.config_file)
    global db
    db = torndb.Connection(options.options.host, options.options.db, options.options.username, options.options.password)
    app.listen(options.options.port)
    ioloop.IOLoop.instance().start()
